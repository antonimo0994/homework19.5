﻿#include <iostream>

class Animal
{
public :
   virtual void Voice()
    {
        std::cout << " ";
    }
};

class Cat : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Meow :3 \n";
    }
};

class Dog : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Woof! \n";
    }
};

class Lion : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Roar!!!! \n";
    }
};


int main()
{
    Animal* animals[3];
    animals[0] = new Cat();
    animals[1] = new Dog();
    animals[2] = new Lion();

    for (Animal* a : animals)
        a->Voice();
}
